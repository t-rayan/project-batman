export class Post {
    public _id: string;
    public title: string;
    public description: string;
    public author: string;
    public created_at: Date;
}