export class User {
    public fullname: string;
    public email: string;
    public username: string;
    public password: string;
    public created_at: Date;
    public isAdmin: boolean;
}