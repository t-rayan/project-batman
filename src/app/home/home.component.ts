import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  posts: Post[] = [];

  constructor(
    private _postService: PostService
  ) { }

  ngOnInit() {
    this.getallPosts();
  }

  getallPosts() {
    this._postService.getPosts()
      .subscribe(data => {
        this.posts = data as Post[];
        console.log(data);
      })
  }


}
