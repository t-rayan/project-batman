import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Post } from '../../models/post';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  post: Post = new Post();

  constructor(
    private _router: ActivatedRoute,
    protected _postService: PostService,
  ) { }

  ngOnInit() {
    this._router.params
      .switchMap((params: Params) => {
        let id = params['id'];
        return this._postService.getPost(id)
      })
      .subscribe(data => {
        this.post = data;
        console.log(data);
      }, err => {
        console.log(err);
      })
  }

}
