import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from '../modules/login/login.component';
import { SignupComponent } from '../modules/signup/signup.component';
import { AdminComponent } from '../modules/admin/admin.component';
import { PostDetailsComponent } from './post-details/post-details.component';

//Route Configuration
export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'post/:id', component: PostDetailsComponent }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
