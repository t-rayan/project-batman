import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { adminRouting } from '../modules/admin/admin.routing';
import { AppComponent } from './app.component';
import { LoginModule } from '../modules/login/login.module';
import { AdminModule } from '../modules/admin/admin.module';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './nav-bar/navbar.component';
import { SignupModule } from '../modules/signup/signup.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { PostService } from '../services/post.service';
import { LoadingModule } from 'ngx-loading';
import { PostDetailsComponent } from './post-details/post-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostDetailsComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    adminRouting,
    LoginModule,
    AdminModule,
    SignupModule,
    FlashMessagesModule,
    LoadingModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
