import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { ValidationService } from '../../services/validation.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any = {};

  constructor(
    private _vs: ValidationService,
    private _fm: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() { }

  onLogin() {
    let user = this.user;
    if (user.username == null || user.username == undefined) {
      this._fm.show("Please Enter Username", {
        cssClass: 'alert-danger', timeout: 5000
      });
    } else if (user.password == null || user.password == undefined) {
      this._fm.show("Please Enter Password", {
        cssClass: 'alert-danger', timeout: 5000
      });
    } else {
      this._authService.authenticateUser(user)
        .subscribe(data => {
          if (data.success) {
            this._authService.storeUserData(data.token, data.user, data.role);
            if (!data.role) {
              this._fm.show("You dont have right priviliges", {
                cssClass: 'alert-danger', timeout: 5000
              });
              this._router.navigate(['/login']);
            } else {
              this._fm.show("You are now logged in", {
                cssClass: 'alert-success', timeout: 5000
              });
              this._router.navigate(['/admin']);
            }
            // this._router.navigate(['/admin']);
          } else {
            this._fm.show(data.msg, {
              cssClass: 'alert-danger', timeout: 5000
            });
            this._router.navigate(['/login']);
          }
        })
    }
  }
}
