import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ValidationService } from '../../services/validation.service';
import { AuthService } from '../../services/auth.service';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SignupComponent],
  exports: [SignupComponent],
  providers: [ValidationService, AuthService]
})
export class SignupModule { }
