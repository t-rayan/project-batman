import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { ValidationService } from '../../services/validation.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {

  user: any = {};
  title: string = '';

  constructor(
    private _vs: ValidationService,
    private _fm: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
   ) {}

  ngOnInit() {
    
  }

  onRegister() {
    if(!this._vs.validateForms(this.user)) {
      this._fm.show('Please fill in all fields', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    } else if(!this._vs.validateEmail(this.user.email)) {
      this._fm.show('Invalid Email', {cssClass: 'alert-danger', timeout: 3000});
    } else {
      
      this._authService.registerUser(this.user)
        .subscribe(data => {
          if(data.success) {
            this._fm.show('User Registration Completed', {cssClass: 'alert-success', timeout: 3000});
            this._router.navigate(['/login']);
          } else {
            this._fm.show('Sorry! Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
            this._router.navigate(['/signup']);
          }
        })
    }
  }

}
