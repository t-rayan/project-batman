import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PostService } from '../../../services/post.service';
import { Post } from '../../../models/post';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  post: Post = new Post();

  constructor(
    private postService: PostService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fm: FlashMessagesService
  ) { }

  ngOnInit() {
    this.activatedRoute.params
      .switchMap((params: Params) => {
        let id = params['id'];

        if(typeof params['id'] !== 'undefined' && params['id'] !== null) {
          return this.postService.getPost(id);
        }
      })
        .subscribe(response => {
          this.post = response;
        }, err => {
          console.log(err);
        });
  }

  //update function
  updatePost() {
    this.postService.updatePost(this.post)
      .subscribe(res => {
        this.router.navigate(['/admin/posts']);
        this.fm.show(res.msg, {cssClass:'alert-success', timeout:3000});
      }, err => {
        console.log(err);
      })
  }
}
