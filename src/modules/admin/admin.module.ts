import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { PostsComponent } from './posts/posts.component';
import { AdminNavBarComponent } from './admin-navbar/admin-navbar.component';
import { UsersComponent } from './users/users.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { adminRouting } from './admin.routing';
import { AuthGuard } from '../../guards/auth.guard';
import { AdminGuard } from '../../guards/admin.guard';
import { UserService } from '../../services/user.service';
import { PostService } from '../../services/post.service';
import { FormsModule }   from '@angular/forms';
import { EditorComponent } from '../../_helpers/editor/editor.component';
import { EditPostComponent } from './edit-post/edit-post.component';

@NgModule({
  imports: [
    CommonModule,
    adminRouting,
    FormsModule,
    
  ],
  declarations: [AdminComponent, PostsComponent, DashboardComponent, UsersComponent, EditorComponent, AdminNavBarComponent, EditPostComponent],
  exports: [AdminComponent, PostsComponent, DashboardComponent, UsersComponent, EditorComponent, AdminNavBarComponent, EditPostComponent ],
  providers: [AuthGuard, AdminGuard, UserService, PostService ]
})
export class AdminModule { }
