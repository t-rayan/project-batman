import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { ValidationService } from '../../services/validation.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  user: any = {};

  constructor(
    private _fm: FlashMessagesService,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    this._authService.getProfile()
      .subscribe(profile => {
        this.user = profile.user;
        console.log(this.user);
      }, err => {
        console.log(err);
        return false;
      });
  }

  onLogout() {
    this._authService.logout();
    this._fm.show("You are logged out", {
      cssClass: 'alert-success',
      timeout: 3000
    });
    this._router.navigate(['/login']);
    return false;
  }

}
