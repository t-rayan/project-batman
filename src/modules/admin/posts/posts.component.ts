import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post.service';
import { Post } from '../../../models/post';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];
  post: Post = new Post();
  message: string;
  newPost = false;
  public loading = false;
  postsLength: number;

  constructor(
    private _postService: PostService,
    private _fm: FlashMessagesService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAllPosts();
  }

  getAllPosts() {
    this._postService.getPosts()
      .subscribe(posts => {
        this.loading = true;
        this.postsLength = posts.length;
        if (posts.length > 0) {
          this.posts = posts as Post[];
        } else {
          this.message = "There is no Post available"
        }
      });
  }

  showaddpostForm() {
    this.newPost = true;
  }

  addPost(post) {
    this._postService.createPost(this.post)
      .subscribe(res => {
        if (res.success) {
          this._fm.show(res.msg, { cssClass: 'alert-success', timeout: 3000 });
          this.newPost = false;
          this.getAllPosts();
        } else {
          console.log(res.err);
        }

      })
  }

  // delete
  delete(id) {
    this._postService.deletePost(id)
      .subscribe(res => {
        this.getAllPosts();
        this._fm.show(res.msg, { cssClass: 'alert-success', timeout: 3000 });
        console.log(res);
      }, err => {
        console.log(err);
      });
  }

  //text-editor function
  TextEditorKeyUp(event) {
    this.post.description = event;
  }

}
