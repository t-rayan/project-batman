import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { User } from '../models/user';


@Injectable()
export class UserService {

  constructor(
    private _http: Http
  ) { }

  getUsers(): Observable<User[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.get('http://localhost:3000/users/all', {headers: headers})
      .map(res => res.json());
  }
}
