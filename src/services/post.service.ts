import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Post } from '../models/post';


@Injectable()
export class PostService {

  constructor(
    private _http: Http
  ) { }

serverUrl = "http://localhost:3000/posts/";

  getPosts(): Observable<Post[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.get('http://localhost:3000/posts', { headers: headers })
      .map(res => res.json());
  }

  createPost(post) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.post('http://localhost:3000/posts', post, { headers: headers })
      .map(res => res.json());
  }

  getPost(id: string): Observable<Post> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.get('http://localhost:3000/posts/' + id, { headers: headers })
      .map(res => res.json());
  }

  // delete post
  deletePost(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.delete('http://localhost:3000/posts/' + id, { headers: headers })
      .map(res => res.json());
  }

  //update post 
  updatePost(post: Post): Observable<any> {
    let headers = new Headers();
    let url = this.serverUrl + post._id;
    headers.append('Content-Type', 'application/json');
    return this._http.put(url, post, { headers: headers })
      .map(res => res.json());
  }
}
