import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {
  
  authToken: any;
  user: any;
  role: boolean;
  baseUrl: 'http://localhost:3000/';

  constructor(
    private _http: Http
  ) { }

  registerUser(user){
    let headers = new Headers();
    let url = this.baseUrl + "/users/register";
    headers.append('Content-Type', 'application/json');
    return this._http.post('http://localhost:3000/users/register', user, {headers: headers})
      .map(res => res.json());
  }

  authenticateUser(user) {
    let headers = new Headers();
    let url = this.baseUrl + "/users/register";
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  getProfile() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    let url = this.baseUrl + "/users/register";
    headers.append('Content-Type', 'application/json');

    return this._http.get('http://localhost:3000/users/profile', {headers: headers})
      .map(res => res.json());
  }

  storeUserData(token, user, role) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('role', role)

    this.authToken = token;
    this.user = user;
    this.role = role
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn() {
    return  !!localStorage.getItem('id_token');
  }

  checkAdmin() {
    return this.user;
  }
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
